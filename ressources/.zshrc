#!/usr/bin/env zsh
#   _________  _   _ ____   ____
#  |__  / ___|| | | |  _ \ / ___|
#    / /\___ \| |_| | |_) | |
# _ / /_ ___) |  _  |  _ <| |___
#(_)____|____/|_| |_|_| \_\\____|
#

# Complétion
autoload -U compinit && compinit
setopt menu_complete
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'
zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                             /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin
zstyle ':completion:*' menu select=2
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
# Crée un cache des complétion possibles
# très utile pour les complétion qui demandent beaucoup de temps
# comme la recherche d'un paquet aptitude install moz<tab>
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh_cache
# des couleurs pour la complétion
# faites un kill -9 <tab><tab> pour voir :)
zmodload zsh/complist
setopt extendedglob
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=36=31"
zstyle ':completion:*:rm:*' ignore-line yes
zstyle ':completion:*:mv:*' ignore-line yes
zstyle ':completion:*:cp:*' ignore-line yes

# Correction des commandes
setopt correctall

# Un petit prompt sympa
autoload -U promptinit
promptinit
prompt adam2

# Affiche des informations sur le système
uname -a
uptime

# Accepte les messages d'autres utilisateurs
mesg y

RPROMPT=' %y %* %D{%d/%m/%Y}'     # prompt for right side of screen

set path='~/ /bin /etc /sbin /usr/bin /usr/etc /usr/include /usr/libexec /usr/sbin /usr /X11R6/bin /usr/bin/X11 /usr/kerberos/bin /usr/local/bin /usr/local/etc /usr/local/include /usr/local/libexec /usr/local/sbin /usr/local/share /usr/share/emacs/site-lisp /usr/local/rsi/idl/bin .'

#set complete

set history=1000
set savehist=1000
set host='/bin/hostname'
set shost='/bin/hostname -s'
set mail='/var/spool/mail/$USER'
set USER_NICKNAME="ludovic post"

limit coredumpsize 0
umask 022
unset autologout

#General setup

HOSTNAME=$HOST
TERM=xterm-256color
EDITOR=emacs
MAIL=$mail
PRINTER=post_l
MANPATH=/usr/bin/man:/usr/kerberos/man:/usr/local/man:/usr/share/man:/usr/X11R6/man:/usr/man


################
# 1. Les alias #
################

# Gestion du 'ls' : couleur & ne touche pas aux accents
alias ls='ls --classify --tabsize=0 --literal --color=auto --show-control-chars --human-readable'

# Raccourcis pour 'ls'
alias la='ls -a'
alias lla='ls -la'

# Quelques alias pratiques
alias c='clear'
alias less='less --quiet'
alias s='cd ..'
alias df='df --human-readable'
alias du='du --human-readable'
alias m='mutt -y'
alias md='mkdir'
alias rd='rmdir'
alias upgrade='apt-get update && apt-get upgrade && apt-get clean'

# Les alias marchent comme sous bash
alias ll='ls --color=auto -lh'
alias lll='ls --color=auto -lh | less'

# marre de se faire corriger par zsh ;)
alias xs='cd'
alias sl='ls'

# Un grep avec des couleurs :
export GREP_COLOR=31
alias grep='grep --color=auto'
alias cd..='cd ..'
alias emacs='emacs -nw'
alias zlock='zlock -pwtext "8======D" -immed'

# Pareil pour les variables d'environement :
#export http_proxy="http://hostname:8080/"
#export HTTP_PROXY=$http_proxy

# Correspondance touches-fonction
bindkey '^A'    beginning-of-line       # Home
bindkey "\e[1~" beginning-of-line
bindkey "\e[H"  beginning-of-line
bindkey '^E'    end-of-line             # End
bindkey "\e[4~" end-of-line
bindkey "\e[F"  end-of-line
bindkey '^D'    delete-char             # Del
bindkey '^[[3~' delete-char
bindkey '^[[2~' overwrite-mode          # Insert
bindkey '^[[5~' history-search-backward # PgUp
bindkey '^[[6~' history-search-forward  # PgDn

# Prompt couleur (la couleur n'est pas la même pour le root et
# pour les simples utilisateurs)
if [ "`id -u`" -eq 0 ]; then
  export PS1="%{^[[36;1m%}%T %{^[[34m%}%n%{^[[33m%}@%{^[[37m%}%m %{^[[32m%}%~ %{^[[33m%}%#%{^[[0m%} "
else
  export PS1="%{^[[36;1m%}%T %{^[[31m%}%n%{^[[33m%}@%{^[[37m%}%m %{^[[32m%}%~ %{^[[33m%}%#%{^[[0m%} "
fi

# Prise en charge des touches [début], [fin] et autres
typeset -A key

key[Home]=${terminfo[khome]}
key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     end-of-line
[[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  overwrite-mode
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char
[[ -n "${key[Up]}"      ]]  && bindkey  "${key[Up]}"      up-line-or-history
[[ -n "${key[Down]}"    ]]  && bindkey  "${key[Down]}"    down-line-or-history
[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    backward-char
[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   forward-char

