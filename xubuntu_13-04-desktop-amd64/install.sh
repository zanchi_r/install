#!/bin/sh

# --- repositories ---
sudo add-apt-repository ppa:webupd8team/sublime-text-2
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
sudo add-apt-repository "deb http://archive.canonical.com/ $(lsb_release -sc) partner"
sudo sh -c 'echo "deb http://repository.spotify.com stable non-free" >> /etc/apt/sources.list.d/google.list'
sudo apt-get update

# --- config de base epitech ---
#emacs
sudo apt-get install -y emacs
echo "-> Copying emacs configuration"
cp ../epitech_ressources/.emacs ~/
sudo cp ../epitech_ressources/site-lisp/* /usr/local/share/emacs/site-lisp/
echo "-> Changing emacs config files permissions"
sudo chmod 644 /usr/local/share/emacs/site-lisp/*
#moulinette de norme
echo "-> Installing moulinette"
sudo cp ../epitech_ressources/norme.py /usr/bin/norme

# --- config zsh ---
sudo apt-get install -y zsh
echo "-> Copying zsh configuration"
cp ../ressources/.zshrc ~/
echo "-> Setting zsh as default"
chsh -s /bin/zsh

# --- dev utilities & libs ---
echo "-> installing dev utilities"
sudo apt-get install -y subversion
sudo apt-get install -y build-essential
sudo apt-get install -y cmake
sudo apt-get install -y valgrind
sudo apt-get install -y qt4-dev-tools
sudo apt-get install -y qtcreator
sudo apt-get install -y eclipse
sudo apt-get install -y sublime-text
sudo apt-get install -y lua5.2
sudo apt-get install -y nodejs
sudo apt-get install -y ocaml
sudo apt-get install -y libsdl-ocaml-dev
sudo apt-get install -y libsdl1.2-dev
sudo apt-get install -y libluabind-dev
sudo apt-get install -y python-pip python-dev

# --- tools ---
sudo apt-get install -y htop
sudo apt-get install -y filezilla

# --- multimedia & internet & bureautique ---
sudo apt-get install -y vlc
sudo apt-get install -y libreoffice
sudo apt-get install -y google-chrome-stable
sudo apt-get install -y skype
sudo apt-get install -y --force-yes spotify-client

cd ~ && wget -O - "https://www.dropbox.com/download?plat=lnx.x86_64" | tar xzf -
~/.dropbox-dist/dropboxd &
