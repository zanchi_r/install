;; BOCAL STAFF 2010-2011
;; FEDORA DUMP

(global-font-lock-mode t)
(column-number-mode t)
(line-number-mode t)
(display-time)
(setq display-time-24hr-format 1)
(load-library "paren")
(show-paren-mode 1)
(add-hook 'write-file-functions 'delete-trailing-whitespace)
(global-set-key [f9] 'query-replace)
(global-set-key [shift f9] 'query-replace-regexp)
(load-file "/usr/local/share/emacs/site-lisp/std_comment.el")

; EOF
(put 'dired-find-alternate-file 'disabled nil)
